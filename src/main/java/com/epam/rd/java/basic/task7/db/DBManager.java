package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static final String URL = "jdbc:mysql://localhost:3306/test2db";
	private static final String USER = "root";
	private static final String PASSWORD = "iwan111223";
	private static final File fileWithUrl = new File("app.properties");
	private static final String FULL_URL = readResourcesFromFile(fileWithUrl) ;


	private static String readResourcesFromFile (File file) {
		String result = null;
		try {
			Scanner myReader = new Scanner(file);
			String InputData = myReader.nextLine();
			myReader.close();
			result = InputData.replaceAll("connection.url=", "");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			synchronized (DBManager.class) {
				if (instance == null)
					instance = new DBManager();
			}
		}
		return instance;
	}

	private DBManager() {

	}

	public List<User> findAllUsers() throws DBException {
		List<User> result = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(FULL_URL);
			 Statement statement = con.createStatement();
			 ResultSet rs = statement.executeQuery("SELECT * FROM users")){
			while (rs.next()) {
				User u = new User();
				u.setId(0);
				u.setLogin(rs.getString("login"));
				result.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("  ",e);
		}
		return result;
	}

	public boolean insertUser(User user) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(FULL_URL);
			stmt = con.prepareStatement("INSERT INTO users (login) VALUES (?)",Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,user.getLogin());
			stmt.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(stmt);
			close(con);
		}
		return true;
	}

	private void close(AutoCloseable stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(FULL_URL);
			stmt = con.prepareStatement("delete from users where login=?", Statement.RETURN_GENERATED_KEYS);
			for (User user : users) {
				stmt.setString(1, user.getLogin());
				stmt.executeUpdate();
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(con);
			close(stmt);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User result = new User();
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(FULL_URL);
			stmt = con.prepareStatement("select * from users where login=?",Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,login);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				result.setLogin(rs.getString("login"));
				result.setId(rs.getInt("id"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(con);
		}
		return result;
	}

	public Team getTeam(String name) throws DBException {
		Team result = new Team();
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(FULL_URL);
			stmt = con.prepareStatement("select * from teams where name=?",Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,name);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				result.setName(rs.getString("name"));
				result.setId(rs.getInt("id"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(stmt);
			close(con);
		}
		return result;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> result = new ArrayList<>();
		try
			(Connection con = DriverManager.getConnection(FULL_URL);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from teams");){
			while (rs.next()) {
				Team t = new Team();
				t.setId(0);
				t.setName(rs.getString("name"));
				result.add(t);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Something went wrong",e);
		} 

		return result;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(FULL_URL);
			stmt = con.prepareStatement("insert into teams (name) values (?)",Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,team.getName());
			stmt.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(stmt);
			close(con);
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement preparedStatement = null;
		User userAnalogFromDB = getUser(user.getLogin());
		try {
			con = DriverManager.getConnection(FULL_URL);
			con.setAutoCommit(false);
			preparedStatement = con.prepareStatement("insert into users_teams (user_id, team_id) values (?,?)",Statement.RETURN_GENERATED_KEYS);
			for (Team team: teams) {
				Team teamAnalogFromDB = getTeam(team.getName());
				preparedStatement.setInt(1, userAnalogFromDB.getId());
				preparedStatement.setInt(2, teamAnalogFromDB.getId());
				preparedStatement.executeUpdate();
			}
			con.commit();
		}catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw  new DBException("",e);
		} finally {
			close(preparedStatement);
			close(con);
		}
		return true;	}

	private void rollback(Connection con) {
		try {
			con.rollback();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> result = new ArrayList<>();
		Connection con = null;
		PreparedStatement stmt = null;
		User userAnalogFromDB = getUser(user.getLogin());
		try {
			con = DriverManager.getConnection(FULL_URL);
			stmt = con.prepareStatement
					("select team_id,teams.name from users_teams join teams\n" +
					"on users_teams.team_id = teams.id\n" +
					"where\n" +
					"user_id = ?", Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1,userAnalogFromDB.getId());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Team t = new Team();
				t.setName(rs.getString(2));
				t.setId(rs.getInt(1));
				result.add(t);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
			close(stmt);
		}
		return result;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(FULL_URL);
			stmt = con.prepareStatement("delete from teams where name=?", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, team.getName());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(con);
			close(stmt);
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(FULL_URL);
			stmt = con.prepareStatement(
					"update teams \n" +
					"set name=?\n" +
					"where name = ?",Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,team.getName());
			stmt.setString(2,team.getOldName());
			stmt.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			close(stmt);
			close(con);
		}
		return true;
	}

}
